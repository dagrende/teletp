var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
const path = require('path');
const debounce = require('lodash.debounce');

const { Pool } = require('pg')
const pool = new Pool({
  connectionString: process.env.DATABASE_URL || 'postgresql://dag:ddg@localhost:5432/dag',
  ssl: {
    rejectUnauthorized: false
  }
})

// if needed, create table with initial positions
pool.connect()
.then(client =>
  client.query('create table position(i integer, x float, y float, plupps varchar(6))')
  .then(res => client.query("insert into position(i, x, y, plupps) values(0, 15, 15, ''),(1, 40, 15, ''),(2, 15, 40, ''),(3, 285, 15, ''),(4, 260, 15, ''),(5, 285, 40, ''),(5, 285, 40, '')"))
  .then(res => client.release())
  .catch(err => {
    if (err.code !== '42P07') {
      // it's not "table already exists error"
      console.log('Error executing create table', err.stack);
    }
    client.release();
  })
)
.catch(err => console.error('Error executing connect', err.stack));

const debouncedSavePos = debounce(function(i, pos) {
  savePos(i, pos);
}, 1000);

// save one position to database
function savePos(i, pos) {
  pool.query('update position set x=$2, y=$3, plupps=$4 where i=$1', [i, pos.x, pos.y, pos.plupps])
  .catch(err => console.error(err));
}

// read all positions from database and send to all browsers
function emitPositions(socket) {
  pool.query('select i, x, y, plupps from position')
  .then(result => result.rows
    .map(row => ({i: row.i, pos: {x: row.x, y: row.y, plupps: row.plupps}, senderId: 'server'}))
    .forEach(item => socket.emit(NEW_POSITION_MESSAGE_TAG, item)))
  .catch(err => console.error(err));
}

app.use(express.static(path.join(__dirname, 'build')));

const NEW_POSITION_MESSAGE_TAG = "newPositionMessage";

let roomPositions = {};
let saveRoomPositions = true;

io.on("connection", (socket) => {
  console.log(`Client ${socket.id} connected`);

  // Join a conversation
  const { roomId } = socket.handshake.query;
  socket.join(roomId);
  if (saveRoomPositions) {
    // send all positions for this room to connecting client
    emitPositions(io.in(roomId));
  }

  // Listen for new messages
  socket.on(NEW_POSITION_MESSAGE_TAG, data => {
    io.in(roomId).emit(NEW_POSITION_MESSAGE_TAG, data);
    if (saveRoomPositions) {
      // save this position
      debouncedSavePos(data.i, data.pos);
    }
  });

  // Leave the room if the user closes the socket
  socket.on("disconnect", () => {
    socket.leave(roomId);
    console.log(`Client ${socket.id} disconnected`);
  });
});

app.use(function (req, res, next) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

let port = process.env.PORT || 4000;
http.listen(port, () => {
  console.log('listening on http://localhost:' + port);
});
