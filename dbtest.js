const { Pool } = require('pg')
const pool = new Pool({
  connectionString: process.env.DATABASE_URL || 'postgresql://dag:ddg@localhost:5432/dag'
})

pool.connect()
.then(client =>
  client.query('create table position(i integer, x float, y float)')
  .then(res => client.query('insert into position(i, x, y) values(0, 15, 15),(1, 15, 15),(2, 15, 15),(3, 15, 15),(4, 15, 15),(5, 15, 15)'))
  .then(res => client.release())
  .catch(err => {
    if (err.code !== '42P07') {
      console.log('Error executing create table', err.stack);
    }
    client.release();
  })
)
.catch(err => console.error('Error executing connect', err.stack));

  // pool.connect()
  //   .then(client => {
  //     client.query('create table position(i integer, x float, y float)')
  //     .then(res => {
  //       client.query('insert into position(i, x, y) values(0, 15, 15),(1, 15, 15),(2, 15, 15),(3, 15, 15),(4, 15, 15),(5, 15, 15)')
  //         .then(res => client.release())
  //         .catch(err => console.error('Error executing insert', err.stack));
  //       // client.release();
  //     })
  //     .catch(err => {
  //       if (err.code !== '42P07') {
  //         console.log('Error executing create table', err.stack);
  //       }
  //       client.release();
  //     });
  //   })
  //   .catch(err => console.error('Error executing connect', err.stack));


// let i = 0;
// let pos = {x: 17, y: 47.11};
//
// pool.connect()
//   .then(client => {
//     client.query('insert into position(i, x, y) values($1, $2, $3)', [i, pos.x, pos.y])
//     .then(res => client.release())
//     .catch(err => console.error('Error executing query', err.stack));
//   })
//   .catch(err => console.error('Error executing connect', err.stack));


// pool
//   .query('insert into position(i, x, y) values($1, $2, $3)', [i, pos.x, pos.y])
//   .then(res => console.log(res))
//   .catch(err => console.error('Error executing query', err.stack));

  // client.query('create table position(i integer, x float, y float)')
  // .then(res => console.log(res))
  // .catch(err => console.error('Error executing query', err.stack));
