import { useEffect, useRef, useState } from "react";
import socketIOClient from "socket.io-client";
import throttle from 'lodash.throttle';

const NEW_POSITION_MESSAGE_TAG = "newPositionMessage";

const usePositions = (roomId, initialPositions) => {
  const [positions, setPositions] = useState(initialPositions);
  const socketRef = useRef();

  var throttledEmitMessage = throttle(function(message){
    socketRef.current.emit(NEW_POSITION_MESSAGE_TAG, message);
  }, 100);

  useEffect(() => {
    let url = window.location.port == 3000 ? "http://localhost:4000" : window.location.href;
    // console.log('window.location', window.location.port, 'url', url);
    socketRef.current = socketIOClient(url, {
      query: { roomId },
    });

    socketRef.current.on(NEW_POSITION_MESSAGE_TAG, ({i, pos, senderId}) => {
      // console.log(i, pos, senderId);
      if (senderId !== socketRef.current.id) {
        setOnePosition(i, pos);
      }
    });

    return () => {
      socketRef.current.disconnect();
    };
  }, [roomId]);

  function setOnePosition(i, pos) {
    setPositions(positions => {
      var newPositions = [...positions];
      newPositions[i] = Object.assign(positions[i] || {}, pos);
      return newPositions;
    });
  }

  const sendPosition = (i, pos) => {
    setOnePosition(i, pos);
    let message = {i, pos: Object.assign(positions[i] || {x: 0, y: 0, plupps: ''}, pos), senderId: socketRef.current.id}
    throttledEmitMessage(message);
  };

  return { positions, sendPosition };
};

export default usePositions;
