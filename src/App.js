import React from "react";
import "./App.css";
import usePositions from "./usePositions";
import {drag} from "d3-drag";
import {select} from "d3-selection";

let latestDragIndex = -1;
let latestPluppIndex = 0;

function App(props) {
  const markerRadius = 10;
  const colors = ['#ee427a', '#18a847', '#c05020', '#0a6b8e', '#f4965d', '#ebe34e']
  const initialPositions = [{x: 15, y: 15, plupps: ''}, {x: 40, y: 15, plupps: ''}, {x: 15, y: 40, plupps: ''},
    {x: 285, y: 15, plupps: ''}, {x: 260, y: 15, plupps: ''}, {x: 285, y: 40, plupps: ''}];
  const { positions, sendPosition } = usePositions('a', initialPositions);

  function keyListener(e) {
    console.log('key', e.keyCode, latestDragIndex);
    // p is 112
    if (e.keyCode == 112 && latestDragIndex != -1) {
      // toggle plupp
      let is = latestPluppIndex.toString();
      let plupps = positions[latestDragIndex].plupps || '';
      if (plupps.indexOf(is) > -1) {
        plupps = plupps.replace(is, '')
      } else {
        plupps = plupps + is;
      }
      sendPosition(latestDragIndex, {plupps: plupps});
    }
  }

  React.useEffect(() => {
    document.addEventListener("keypress",  keyListener);
    return () => document.removeEventListener("keypress",  keyListener);
  }, []);

  function Marker(props) {
    const nodeRef = React.useRef(null);

    React.useEffect(() => {
      let handleDrag = drag()
        .subject(function() {
          const me = select(this);
          const transform = me.attr('transform');
          const matchList = transform.match(/translate\((.*) (.*)\)/);
          let  result = { x: parseFloat(matchList[1]), y: parseFloat(matchList[2]) };
          return result;
        }).on('start', function(d){
          console.log('drag start');
          latestDragIndex = props.id;
        })
        .on('drag', function(d) {
          sendPosition(props.id, {x: d.x, y: d.y});
        })
        .on('end', function(d){
          console.log('drag end');
          latestPluppIndex = Math.floor(((510 + Math.atan2(d.y - 150, d.x - 150) * 180 / Math.PI) % 360) / 60);
        });
      const node = nodeRef.current;
      handleDrag(select(node));
    }, [props.id]);
    return (
      <g ref={nodeRef} transform={'translate('+ props.x + ' ' + props.y + ')'}>
        <circle r={markerRadius} fill={props.color}/>
        {colors.map((color, j) => (
          <g key={j} transform={'rotate(' + (60 * j) + ')'}>
            <path className="plupp" d={wedgePath(markerRadius - 0.5)}
                fill={props.plupps.includes(j.toString()) ? color : darken(props.color)} stroke={props.color} strokeWidth="1" />
          </g>
        ))}
      </g>
    );
  }

  function darken(color) {
    const darkenComponent = c => ('0'+Math.floor(parseInt(c, 16)/1.2).toString(16)).slice(-2)
    return '#' +
        darkenComponent(color.substring(1, 3)) +
        darkenComponent(color.substring(3, 5)) +
        darkenComponent(color.substring(5, 7));
  }

  function wedgePath(r) {
    return "M0,0 L" + r + ",0" +
      " A" + r + "," + r + " 0 0,1 " + (r * 0.5) + "," + (r * 0.866) + " z";
  }

  return (
    <div className="tp-room-container">
      <div className="positions-container">
        <svg id="board" version="1.1" preserveAspectRatio="xMidYMid meet"
            viewBox="0 0 300 300"
            baseProfile="full" fill="black"
            xmlns="http://www.w3.org/2000/svg">
          <image href="tp-deluxe-board.jpg" height="300" width="300"/>
          {positions.map((position, i) => (
              <Marker
                className={'marker'}
                key={i} id={i}
                x={position.x} y={position.y} plupps={position.plupps}
                color={colors[i]}
              />
          ))}
        </svg>
        ))}
      </div>
    </div>
  );
};

export default App;
